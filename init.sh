#!/bin/bash

command -v ansible-playbook >/dev/null 2>&1 || {
    sudo apt-add-repository -y ppa:ansible/ansible
    sudo apt update
    sudo apt install -y ansible
}

ansible-playbook playbook.yaml
